USE [master]
GO
/****** Object:  Database [TP]    Script Date: 24/11/2022 13:09:11 ******/
CREATE DATABASE [TP]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TP', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\TP.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'TP_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\TP_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [TP] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TP] SET ARITHABORT OFF 
GO
ALTER DATABASE [TP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TP] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TP] SET  MULTI_USER 
GO
ALTER DATABASE [TP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TP] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TP] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [TP] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [TP] SET QUERY_STORE = OFF
GO
USE [TP]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 24/11/2022 13:09:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[id] [int] NOT NULL,
	[username] [varchar](50) NOT NULL,
	[pass] [varchar](50) NOT NULL,
	[partidas_jugadas] [int] NOT NULL,
	[partidas_ganadas] [int] NOT NULL,
	[tiempo_jugado] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[usuario] ADD  CONSTRAINT [DF_usuario_partidas_jugadas]  DEFAULT ((0)) FOR [partidas_jugadas]
GO
ALTER TABLE [dbo].[usuario] ADD  CONSTRAINT [DF_usuario_partidas_ganadas]  DEFAULT ((0)) FOR [partidas_ganadas]
GO
ALTER TABLE [dbo].[usuario] ADD  CONSTRAINT [DF_usuario_tiempo_jugado]  DEFAULT ((0)) FOR [tiempo_jugado]
GO
/****** Object:  StoredProcedure [dbo].[usuario_buscar_por_username]    Script Date: 24/11/2022 13:09:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[usuario_buscar_por_username]
@user varchar(50)
as
begin 

select id, username, pass, partidas_jugadas, partidas_ganadas, tiempo_jugado 
from usuario where username = @user 
end

GO
/****** Object:  StoredProcedure [dbo].[usuario_editar]    Script Date: 24/11/2022 13:09:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usuario_editar]
@id int ,
@jugadas int,
@ganadas int,
@tiempo int
as
begin 
update usuario set
partidas_jugadas = @jugadas,
partidas_ganadas = @ganadas,
tiempo_jugado = @tiempo

where id = @id
end

GO
/****** Object:  StoredProcedure [dbo].[usuario_insertar]    Script Date: 24/11/2022 13:09:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usuario_insertar]
@user varchar(50),
@pass varchar(50)
as
begin 

declare @id int 
set @id = (select isnull(max(id),0) + 1 from usuario)

insert into usuario (id, username, pass) values (@id, @user, @pass)
end
GO
/****** Object:  StoredProcedure [dbo].[usuario_listar]    Script Date: 24/11/2022 13:09:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usuario_listar]
as
begin 
select id, username, partidas_jugadas, partidas_ganadas, tiempo_jugado
from usuario
order by partidas_ganadas desc
end

GO
USE [master]
GO
ALTER DATABASE [TP] SET  READ_WRITE 
GO
