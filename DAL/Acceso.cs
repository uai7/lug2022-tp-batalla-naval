﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    internal class Acceso
    {
        private SqlConnection connection;
        private SqlTransaction tx;
        public void Abrir()
        {
            connection = new SqlConnection(@"initial catalog = TP; data source=.\SQLEXPRESS ; integrated security = sspi");
            connection.Open();

        }
        public void Cerrar()
        {
            connection.Close();
            connection = null;
            GC.Collect();
        }

        public void IniciarTX()
        {
            tx = connection.BeginTransaction();
        }

        public void ConfirmarTx()
        {
            tx.Commit();
            tx = null;
        }

        public void RollbackTx()
        {
            tx.Rollback();
            tx = null;
        }


        private SqlCommand CrearComando(string sp, List<SqlParameter> parametros = null)
        {
            SqlCommand cmd = new SqlCommand(sp, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            if (parametros != null)
            {
                cmd.Parameters.AddRange(parametros.ToArray());
            }
            if (tx != null)
            {
                cmd.Transaction = tx;

            }
            return cmd;

        }

        public DataTable Leer(string sp, List<SqlParameter> parametros = null)
        {
            DataTable tabla = new DataTable();
            using (SqlDataAdapter da = new SqlDataAdapter())
            {
                da.SelectCommand = CrearComando(sp, parametros);
                da.Fill(tabla);
                da.Dispose();
            }

            return tabla;

        }

        public int LeerEscalar(string sp, List<SqlParameter> parametros = null)
        {
            int resultado;
            SqlCommand cmd = CrearComando(sp, parametros);
            try
            {
                resultado = int.Parse(cmd.ExecuteScalar().ToString());
            }
            catch
            {
                resultado = -1;
            }
            return resultado;

        }


        public int Escribir(string sp, List<SqlParameter> parametros = null)
        {
            int resultado;
            SqlCommand cmd = CrearComando(sp, parametros);
            try
            {
                resultado = cmd.ExecuteNonQuery();
            }
            catch
            {
                resultado = -1;
            }
            return resultado;

        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.String;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.Int32;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, float valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.Single;
            return parametro;
        }

    }

}
