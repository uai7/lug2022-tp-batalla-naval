﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Mp_Usuario : Mapper<Usuario>
    {
        public override Usuario ToEntidad(DataRow registro)
        {
            Usuario user = new Usuario();
            user.Id = int.Parse(registro["id"].ToString());
            user.Username = registro["username"].ToString();
            user.Pass = registro.Table.Columns.Contains("pass") ? registro["pass"]?.ToString() : null;
            user.PartidasJugadas = int.Parse( registro["partidas_jugadas"].ToString());
            user.PartidasGanadas = int.Parse(registro["partidas_ganadas"].ToString());
            user.TiempoJugado = int.Parse(registro["tiempo_jugado"].ToString());
            return user;
        }
        public override List<Usuario> Listar()
        {

            acceso.Abrir();
            DataTable tabla = acceso.Leer("usuario_listar");
            acceso.Cerrar();

            List<Usuario> users = new List<Usuario>();
            
            foreach(DataRow registro in tabla.Rows)
            {
                users.Add(ToEntidad(registro));
            }

            return users;
        }

        public Usuario BuscarPorUsername(string username)
        {

            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@user", username));

            DataTable tabla = acceso.Leer("usuario_buscar_por_username", parametros);
            acceso.Cerrar();

            if (tabla.Rows.Count == 0)
            {
                return null;
            }

            return ToEntidad(tabla.Rows[0]);
        }

        public override int Insertar(Usuario entidad)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@user", entidad.Username));
            parametros.Add(acceso.CrearParametro("@pass", entidad.Pass));

            acceso.Abrir();
            int res = acceso.Escribir("usuario_insertar", parametros);
            acceso.Cerrar();
            return res;
        }

        public override int Editar(Usuario entidad)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@id", entidad.Id));
            parametros.Add(acceso.CrearParametro("@jugadas", entidad.PartidasJugadas));
            parametros.Add(acceso.CrearParametro("@ganadas", entidad.PartidasGanadas));
            parametros.Add(acceso.CrearParametro("@tiempo", entidad.TiempoJugado));

            acceso.Abrir();
            int res = acceso.Escribir("usuario_editar", parametros);
            acceso.Cerrar();
            return res;
        }
    }
}
