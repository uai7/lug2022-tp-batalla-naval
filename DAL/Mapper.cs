﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public abstract class Mapper<T> 
    {
        internal Acceso acceso = new Acceso();

        public abstract List<T> Listar();
        public abstract int Insertar(T entidad);
        public abstract int Editar(T entidad);



        public abstract T ToEntidad(DataRow registro);
    }
}
