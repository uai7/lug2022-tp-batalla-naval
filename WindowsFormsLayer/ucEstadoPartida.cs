﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class ucEstadoPartida : UserControl
    {
        private Partida _p;

        private Label _estadoPartida;
        private Label _jugador;
        private Label _ganador;
        private Button _btnReiniciar;
        private ListBox _lbTurnos;
        private Timer _timer;

        public ucEstadoPartida(Partida p)
        {
            InitializeComponent();
            _p = p;
        }

        public void Refrescar()
        {
            List<string> turnos = new List<string>();
            for (int i = 0; i < _p.Turnos.Count; i++)
            {
                turnos.Add("Turno " + (i + 1).ToString() + ": " + _p.Turnos[i].Jugador.Nombre + " => " + _p.Turnos[i].Coordenada.ToString() + " => " + _p.Turnos[i].Resultado);
            }
            turnos.Reverse();
            _lbTurnos.DataSource = null;
            _lbTurnos.DataSource = turnos;

            UpdateTime();

            if (_p.Estado == EstadoPartida.FINALIZADA)
            {
                _timer.Stop();
                _timer.Dispose();

                _ganador.Text = "Ganador: " + _p.JugadorGanador?.Nombre;
                _ganador.Visible = true;
                _btnReiniciar.Visible = true;
            }

            _jugador.Text = "Jugador Actual: " + _p.JugadorAtacando.Nombre;

            this.Refresh();

        }

        private void ucEstadoPartida_Load(object sender, EventArgs e)
        {
            _timer = new Timer();
            _timer.Interval = (1000);
            _timer.Enabled = true;
            _timer.Start();
            _timer.Tick += new EventHandler(timer_Tick);

            _estadoPartida = new Label();
            _estadoPartida.AutoSize = true;
            _estadoPartida.Font = new Font("Bahnschrift SemiCondensed", 14, FontStyle.Bold);

            _jugador = new Label();
            _jugador.AutoSize = true;
            _jugador.Font = new Font("Bahnschrift SemiCondensed", 12, FontStyle.Regular);
            _jugador.Location = new Point(0, _estadoPartida.Size.Height + 10);

            _ganador = new Label();
            _ganador.AutoSize = true;
            _ganador.Font = new Font("Bahnschrift SemiCondensed", 12, FontStyle.Regular);
            _ganador.Location = new Point(0, _jugador.Location.Y + _jugador.Size.Height + 10);
            _ganador.Visible = false;

            _btnReiniciar = new Button();
            _btnReiniciar.Size = new Size(200, 30);
            _btnReiniciar.AutoSize = false;
            _btnReiniciar.Text = "NUEVA PARTIDA";
            _btnReiniciar.Font = new Font("Bahnschrift SemiCondensed", 12, FontStyle.Bold);
            _btnReiniciar.Location = new Point(0, _ganador.Location.Y + _ganador.Size.Height + 10);
            _btnReiniciar.Click += btnReiniciar_Click;
            _btnReiniciar.Visible = false;


            _lbTurnos = new ListBox();
            _lbTurnos.Location = new Point(Size.Width - 400, 0);
            _lbTurnos.Size = new Size(400, Size.Height);
            _lbTurnos.Font = new Font("Bahnschrift SemiCondensed", 10, FontStyle.Regular);


            this.Controls.Add(_estadoPartida);
            this.Controls.Add(_jugador);
            this.Controls.Add(_lbTurnos);
            this.Controls.Add(_ganador);
            this.Controls.Add(_btnReiniciar);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            UpdateTime();
        }

        private void UpdateTime()
        {
            if (_p.StartTime != null && _p.Estado == EstadoPartida.EN_CURSO)
            {
                TimeSpan ts = DateTime.Now.Subtract(_p.StartTime);

                _estadoPartida.Text = "Duración  " + Math.Floor(ts.TotalMinutes).ToString().PadLeft(2, '0') + " : " + ts.Seconds.ToString().PadLeft(2, '0');
            }
        }

        private void btnReiniciar_Click(object sender, System.EventArgs e)
        {
            frmJuego frm = (frmJuego)this.Parent;
            frm.NuevaPartida();
        }
    }
}
