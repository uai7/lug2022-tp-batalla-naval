﻿using BE;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class ucCelda : UserControl
    {

        private PictureBox _pb;
        private Celda _celda;
        private TableroType _type;

        public ucCelda(Celda celda, TableroType type)
        {
            InitializeComponent();

            _celda = celda;
            _type = type;

            Bitmap img = GetBaseImage();

            _pb = new PictureBox
            {
                Size = this.Size,
                Location = new Point(0, 0),
                SizeMode = PictureBoxSizeMode.StretchImage,
                Image = img.Clone(
                    new Rectangle(0, 0, img.Width, img.Height),
                    System.Drawing.Imaging.PixelFormat.DontCare),
                Visible = true,
            };
            _pb.MouseEnter += pb_MouseEnter;
            _pb.MouseLeave += pb_MouseLeave;
            _pb.Click += pb_Click;



            this.Controls.Add(_pb);

            img.Dispose();


        }

        public void Refrescar()
        {
            _pb.Image.Dispose();
            _pb.Image = null;

            Bitmap img = GetBaseImage();

            _pb.Image = img.Clone(
                    new Rectangle(0, 0, img.Width, img.Height),
                   PixelFormat.DontCare);

            img.Dispose();
        }

        private Bitmap GetBaseImage()
        {
            List<Bitmap> images = new List<Bitmap>();

            images.Add(new Bitmap(@"img\water.png"));

            Bitmap barco;


            if (_celda.Barco != null)
            {
                if (_celda.Barco.Orientacion == Orientacion.Vertical)
                {
                    _celda.Barco.CeldasOcupadas.Sort((a, b) => b.Coordenada.Y - a.Coordenada.Y);
                }
                else
                {
                    _celda.Barco.CeldasOcupadas.Sort((a, b) => a.Coordenada.X - b.Coordenada.X);
                }

                int index = _celda.Barco.CeldasOcupadas.IndexOf(_celda) + 1;

                switch (_celda.Barco.Tipo)
                {
                    case TipoBarco.BATTLESHIP:
                        {
                            barco = new Bitmap(@"img\ships\battleship\battleship-" + index + ".png");
                            break;
                        }
                    case TipoBarco.PORTAVIONES:
                        {
                            barco = new Bitmap(@"img\ships\planecarrier\planecarrier-" + index + ".png");

                            break;
                        }
                    case TipoBarco.DESTRUCTOR:
                        {
                            barco = new Bitmap(@"img\ships\destroyer\destroyer-" + index + ".png");

                            break;
                        }
                    case TipoBarco.SUBMARINO:
                        {
                            barco = new Bitmap(@"img\ships\submarine\submarine-" + index + ".png");

                            break;
                        }
                    case TipoBarco.PATRULLA:
                        {
                            barco = new Bitmap(@"img\ships\patrol\patrol-" + index + ".png");

                            break;
                        }
                    default:
                        throw new System.Exception("Unknown ship!");
                }


                if (_celda.Barco.Orientacion == Orientacion.Vertical)
                {
                    barco.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }

                images.Add(barco);
            }

            if (_celda.Atacada)
            {
                bool hit = _celda.Barco != null 
                    || _celda.ResultadoAtaque == ResultadoTurno.HUNDIDO 
                    || _celda.ResultadoAtaque == ResultadoTurno.TOCADO;
                images.Add(new Bitmap(hit ? @"img\hit.png" : @"img\missed.png"));
            }

            Bitmap final = new Bitmap(images[0].Width, images[0].Height, PixelFormat.Format32bppArgb);

            Graphics graphics = Graphics.FromImage(final);
            graphics.CompositingMode = CompositingMode.SourceOver;

            foreach (Bitmap img in images)
            {
                graphics.DrawImage(img, 0, 0, final.Width, final.Height);
                img.Dispose();
            }


            graphics.Dispose();
            return final;
        }


        private void pb_MouseEnter(object sender, System.EventArgs e)
        {
            frmJuego frm = (frmJuego)this.Parent.Parent;


            if (_type == TableroType.DEFENSA && frm.EstadoPartida == EstadoPartida.PREPARACION)
            {
                if (frm.BarcoSeleccionado != null)
                {
                    frm.ColocarBarcoSeleccionado(_celda.Coordenada);
                }
            }
            else if (_type == TableroType.ATAQUE && frm.EstadoPartida == EstadoPartida.EN_CURSO)
            {

            }
        }

        private void pb_MouseLeave(object sender, System.EventArgs e)
        {
            frmJuego frm = (frmJuego)this.Parent.Parent;

            if (_type == TableroType.DEFENSA && frm.EstadoPartida == EstadoPartida.PREPARACION)
            {
                if (frm.BarcoSeleccionado != null)
                {
                    frm.RemoverBarco();
                }
            }
            else if (_type == TableroType.ATAQUE && frm.EstadoPartida == EstadoPartida.EN_CURSO)
            {

            }
        }

        private void pb_Click(object sender, System.EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            frmJuego frm = (frmJuego)this.Parent.Parent;

            if (_type == TableroType.DEFENSA && frm.EstadoPartida == EstadoPartida.PREPARACION)
            {
                if (me.Button == MouseButtons.Left)
                {
                    frm.SeleccionarBarco(frm.BarcoSeleccionado == null && _celda.Barco != null ? _celda.Barco : null);
                }
                else if (me.Button == MouseButtons.Right)
                {
                    frm.orientacion = frm.orientacion == Orientacion.Vertical ? Orientacion.Horizontal : Orientacion.Vertical;
                    if (frm.BarcoSeleccionado != null)
                    {
                        frm.RemoverBarco();
                        frm.ColocarBarcoSeleccionado(_celda.Coordenada);
                    }
                }

            }
            else if (_type == TableroType.ATAQUE && frm.EstadoPartida == EstadoPartida.EN_CURSO)
            {
                frm.Atacar(_celda.Coordenada);
                Refrescar();
            }
        }
    }
}
