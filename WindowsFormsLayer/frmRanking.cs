﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class frmRanking : Form
    {

        private BLL.Usuario gestorUsuarios = new BLL.Usuario();

        public frmRanking()
        {
            InitializeComponent();
        }

        private void frmRanking_Load(object sender, EventArgs e)
        {
            Left = (MdiParent.ClientRectangle.Width - Width) / 2;
            Top = (MdiParent.ClientRectangle.Height - Height) / 2;

            List<BE.Usuario> users = gestorUsuarios.Listar();

            dgvRanking.DataSource = users;

            dgvRanking.Columns["Id"].Visible = false;
            dgvRanking.Columns["TiempoJugado"].Visible = false;
            dgvRanking.Columns["Pass"].Visible = false;

            dgvRanking.Columns["Username"].HeaderText = "Usuario";
            dgvRanking.Columns["PartidasJugadas"].HeaderText = "Partidas Jugadas";
            dgvRanking.Columns["PartidasGanadas"].HeaderText = "Partidas Ganadas";
            dgvRanking.Columns["TiempoJugadoString"].HeaderText = "Tiempo total de juego";


        }
    }
}
