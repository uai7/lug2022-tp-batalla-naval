﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class frmLogin : Form
    {
        private BLL.Usuario gestorUsuarios = new BLL.Usuario();
        public frmLogin()
        {
            InitializeComponent();

        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {

            if(!ValidarInputs())
            {
                MessageBox.Show("Complete los campos");
                return;
            }

            BE.Usuario user = gestorUsuarios.BuscarPorUsername(txtUser.Text);

            if(user == null)
            {
                MessageBox.Show("El usuario no existe");
                return;
            }

            if(user.Pass != txtPass.Text)
            {
                MessageBox.Show("La contraseña es incorrecta");
                return;
            }

            txtUser.Text = "";
            txtPass.Text = "";
            ((frmPrincipal)MdiParent).IniciarSesion(user);
            Close();
        }

        private bool ValidarInputs()
        {
            return (!string.IsNullOrWhiteSpace(txtUser.Text) && !string.IsNullOrWhiteSpace(txtPass.Text));

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            Left = (MdiParent.ClientRectangle.Width - Width) / 2;
            Top = (MdiParent.ClientRectangle.Height - Height) / 2;
        }

        private void btnCrearUsuario_Click(object sender, EventArgs e)
        {
            ((frmPrincipal)MdiParent).MostrarRegistroUsuario();

        }
    }
}
