﻿using BE;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class frmJuego : Form
    {
        static Random rand = new Random();

        Partida p;
        ucTablero _tableroDefensa;
        ucTablero _tableroAtaque;
        ucTablero _tableroAtaqueResultado;
        ucFlota _ucFlota;
        ucEstadoPartida _ucEstado;

        private int _delayAtaque = 500;

        public Barco BarcoSeleccionado;
        public Orientacion orientacion = Orientacion.Horizontal;
        public EstadoPartida EstadoPartida { get => p.Estado; }

        private BLL.Usuario gestorUsuarios = new BLL.Usuario();

        public frmJuego()
        {
            InitializeComponent();

            Size = new Size(780, 560);
        }

        private void frmJuego_Load(object sender, EventArgs e)
        {
            Left = (MdiParent.ClientRectangle.Width - Width) / 2;
            Top = (MdiParent.ClientRectangle.Height - Height) / 2;

            NuevaPartida();
        }

        public void NuevaPartida()
        {
            if (_ucFlota != null) Controls.Remove(_ucFlota);
            if (_ucEstado != null) Controls.Remove(_ucEstado);
            if (_tableroDefensa != null) Controls.Remove(_tableroDefensa);
            if (_tableroAtaque != null) Controls.Remove(_tableroAtaque);
            if (_tableroAtaqueResultado != null) Controls.Remove(_tableroAtaqueResultado);

            _ucFlota?.Dispose();
            _ucEstado?.Dispose();
            _tableroDefensa?.Dispose();
            _tableroAtaque?.Dispose();
            _tableroAtaqueResultado?.Dispose();

            p = new Partida();
            p.Jugador1.User = ((frmPrincipal)MdiParent).UserLogueado;

            BarcoSeleccionado = null;
            orientacion = Orientacion.Horizontal;

            _ucFlota = new ucFlota(p.Jugador1.Barcos);
            _ucFlota.Location = new Point(20, 10);
            _ucFlota.Size = new Size(Size.Width - 60, 140);

            _ucEstado = new ucEstadoPartida(p);
            _ucEstado.Location = _ucFlota.Location;
            _ucEstado.Size = _ucFlota.Size;
            _ucEstado.Visible = false;

            _tableroDefensa = new ucTablero(p.Jugador1.TableroDefensa, TableroType.DEFENSA);
            _tableroDefensa.Location = new Point(20, _ucFlota.Size.Height + _ucFlota.Location.Y + 20);

            _tableroAtaque = new ucTablero(p.Jugador1.TableroAtaque, TableroType.ATAQUE);
            _tableroAtaque.Location = new Point(400, _ucFlota.Size.Height + _ucFlota.Location.Y + 20);


            Controls.Add(_ucFlota);
            Controls.Add(_ucEstado);
            Controls.Add(_tableroDefensa);
            Controls.Add(_tableroAtaque);
        }

        public void SeleccionarBarco(Barco b)
        {
            BarcoSeleccionado = BarcoSeleccionado == b ? null : b;
            _ucFlota.Refrescar();
        }

        public void ColocarBarcoSeleccionado(Coordenada coor)
        {
            if (BarcoSeleccionado != null)
            {
                p.Jugador1.ColocarBarco(BarcoSeleccionado, coor, orientacion);
                _tableroDefensa.Refrescar();
            }
        }

        public void RemoverBarco()
        {
            if (BarcoSeleccionado != null)
            {
                p.Jugador1.RemoverBarco(BarcoSeleccionado);
                _tableroDefensa.Refrescar();
            }
        }


        public void IniciarPartida()
        {
            if (p.Jugador1.EstaListo && EstadoPartida == EstadoPartida.PREPARACION)
            {
                _ucFlota.Visible = false;


                foreach (Barco b in p.Jugador2.Barcos)
                {
                    bool colocado = false;

                    while (!colocado)
                    {
                        Coordenada c = new Coordenada(rand.Next(1, Partida.TableroSize), rand.Next(1, Partida.TableroSize));
                        Orientacion o = rand.Next(0, 2) == 0 ? Orientacion.Horizontal : Orientacion.Vertical;

                        colocado = p.Jugador2.ColocarBarco(b, c, o);
                    }
                }

                p.IniciarPartida();

                _ucEstado.Visible = true;
                _ucEstado.Refrescar();

            }
        }

        public void Atacar(Coordenada coord)
        {
            if (p.JugadorAtacando != p.Jugador1) return;

            p.Atacar(coord);

            _tableroDefensa.Refrescar();
            _ucEstado.Refrescar();

            CheckearGanador();

            if (p.Estado == EstadoPartida.EN_CURSO)
            {
                AtaqueJugador2();
            }
        }

        private async void AtaqueJugador2()
        {
            await Task.Delay(_delayAtaque);

            List<Celda> celdasConBarcos = p.Jugador2.TableroAtaque.Celdas
                .Where(c => c.Atacada && c.ResultadoAtaque == ResultadoTurno.TOCADO)
                .ToList();

            List<Celda> posiblesCeldas = new List<Celda>();

            celdasConBarcos.ForEach(c =>
            {
                List<Celda> celdasVecinas = GetCeldasVecinas(c.Coordenada, p.Jugador2.TableroAtaque);

                celdasVecinas.Where(cv => !cv.Atacada).ToList()
                    .ForEach(cv => { if (!posiblesCeldas.Contains(cv)) posiblesCeldas.Add(cv); });
            });

            if (posiblesCeldas.Count == 0)
            {
                posiblesCeldas = p.Jugador2.TableroAtaque.Celdas
                    .Where(c => (c.Coordenada.X % 2 == 0 && c.Coordenada.Y % 2 == 0) || (c.Coordenada.X % 2 == 1 && c.Coordenada.Y % 2 == 1))
                    .Where(c => !c.Atacada)
                    .ToList();
            }

            if (posiblesCeldas.Count == 0)
            {
                posiblesCeldas = p.Jugador2.TableroAtaque.Celdas
                    .Where(c => !c.Atacada)
                    .ToList();
            }




            // Ataque jugador 2
            Celda celda = posiblesCeldas[rand.Next(0, posiblesCeldas.Count)];
            p.Atacar(celda.Coordenada);

            _tableroDefensa.Refrescar();
            _ucEstado.Refrescar();

            CheckearGanador();
        }

        private List<Celda> GetCeldasVecinas(Coordenada coor, Tablero tablero)
        {
            List<Celda> _celdas = new List<Celda>();

            if (coor.X > 1) _celdas.Add(tablero.ObtenerCelda(new Coordenada(coor.X - 1, coor.Y)));
            if (coor.X < Partida.TableroSize) _celdas.Add(tablero.ObtenerCelda(new Coordenada(coor.X + 1, coor.Y)));

            if (coor.Y > 1) _celdas.Add(tablero.ObtenerCelda(new Coordenada(coor.X, coor.Y - 1)));
            if (coor.Y < Partida.TableroSize) _celdas.Add(tablero.ObtenerCelda(new Coordenada(coor.X, coor.Y + 1)));

            return _celdas;
        }

        private void CheckearGanador()
        {
            if (p.Estado == EstadoPartida.FINALIZADA)
            {
                _tableroAtaqueResultado = new ucTablero(p.Jugador2.TableroDefensa, TableroType.ATAQUE);
                _tableroAtaqueResultado.Location = _tableroAtaque.Location;
                Controls.Add(_tableroAtaqueResultado);

                _tableroAtaque.Visible = false;

                MessageBox.Show("La partida finalizó! El ganador es: " + p.JugadorGanador.Nombre);

                gestorUsuarios.Grabar(p.Jugador1.User);
            }
        }
    }
}
