﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class frmRegistro : Form
    {
        private BLL.Usuario gestorUsuarios = new BLL.Usuario();

        public frmRegistro()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            
            if (!ValidarInputs())
            {
                MessageBox.Show("Complete los campos!");
                return;
            }

            if(txtPass.Text != txtRepeatPass.Text)
            {
                MessageBox.Show("Las contraseñas no coinciden!");
                return;
            }

            if (txtPass.Text.Length < 3)
            {
                MessageBox.Show("La contraseña debe tener 3 caracteres como mínimo!");
                return;
            }


            BE.Usuario user = gestorUsuarios.BuscarPorUsername(txtUser.Text);

            if(user != null)
            {
                MessageBox.Show("El usuario ya se encuentra registrado!");
                return;
            }

            BE.Usuario newUser = new BE.Usuario();
            newUser.Username = txtUser.Text;
            newUser.Pass = txtPass.Text;

            gestorUsuarios.Grabar(newUser);

            MessageBox.Show("El usuario se creó correctamente.");

            txtUser.Text = "";
            txtPass.Text = "";
            txtRepeatPass.Text = "";
            ((frmPrincipal)MdiParent).MostrarLogin();

        }


        private bool ValidarInputs()
        {
            return !string.IsNullOrWhiteSpace(txtUser.Text) && !string.IsNullOrWhiteSpace(txtPass.Text) && !string.IsNullOrWhiteSpace(txtRepeatPass.Text);

        }

        private void frmRegistro_Load(object sender, EventArgs e)
        {
            Left = (MdiParent.ClientRectangle.Width - Width) / 2;
            Top = (MdiParent.ClientRectangle.Height - Height) / 2;
        }
    }
}
