﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class ucBarco : UserControl
    {
        private Barco _barco;
        private PictureBox _pb;

        public Barco Barco { get => _barco; }

        public ucBarco(Barco barco)
        {
            InitializeComponent();

            _barco = barco;

            Bitmap imgBarco;

            switch (barco.Tipo)
            {
                case TipoBarco.BATTLESHIP:
                    imgBarco = new Bitmap(@"img/ships/battleship/battleship.png");
                    break;
                case TipoBarco.PORTAVIONES:
                    imgBarco = new Bitmap(@"img/ships/planecarrier/planecarrier.png");
                    break;
                case TipoBarco.DESTRUCTOR:
                    imgBarco = new Bitmap(@"img/ships/destroyer/destroyer.png");
                    break;
                case TipoBarco.SUBMARINO:
                    imgBarco = new Bitmap(@"img/ships/submarine/submarine.png");
                    break;
                case TipoBarco.PATRULLA:
                    imgBarco = new Bitmap(@"img/ships/patrol/patrol.png");
                    break;
                default:
                    throw new System.Exception("Unknown ship!");
            }

            _pb = new PictureBox
            {
                Location = new Point(0, 0),
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(barco.Largo * 30, 30),
                Image = imgBarco.Clone(
                    new Rectangle(0, 0, imgBarco.Width, imgBarco.Height),
                    System.Drawing.Imaging.PixelFormat.DontCare),
            };
            _pb.Click += pbBarco_Click;
            Controls.Add(_pb);
            imgBarco.Dispose();
        }

        private void pbBarco_Click(object sender, EventArgs e)
        {
            frmJuego frm = (frmJuego)this.Parent.Parent;
            frm.SeleccionarBarco(_barco);
        }

    }
}
