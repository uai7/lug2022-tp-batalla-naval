﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsLayer;

namespace WindowsFormsLayer
{
    public partial class frmPrincipal : Form
    {
        private BE.Usuario _userLogueado;
        private frmLogin _login;
        private frmJuego _juego;
        private frmRegistro _registro;
        private frmRanking _ranking;

        public Usuario UserLogueado { get => _userLogueado; }

        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            btnCerrarSesion.Visible = false;
            btnJuego.Visible = false;
            btnRanking.Visible = true;
            btnLogin.Visible = true;
            MostrarLogin();
        }

        public void MostrarLogin()
        {
            if (_login?.Visible == true) return;

            if (_login == null)
            {
                _login = new frmLogin();
                _login.MdiParent = this;
            }

            _registro?.Hide();
            _juego?.Hide();
            _ranking?.Hide();
            _login.Show();
        }

        private void MostrarJuego()
        {
            if (_juego?.Visible == true) return;

            if (_juego == null)
            {
                _juego = new frmJuego();
                _juego.MdiParent = this;
            }

            _login?.Hide();
            _registro?.Hide();
            _ranking?.Hide();
            _juego.Show();
        }

        private void MostrarRanking()
        {
            if (_ranking?.Visible == true) return;


            _ranking = new frmRanking();
            _ranking.MdiParent = this;


            _login?.Hide();
            _registro?.Hide();
            _juego?.Hide();
            _ranking.Show();
        }

        private void frmPrincipal_Resize(object sender, EventArgs e)
        {
            CenterForms();
        }
        private void CenterForms()
        {
            foreach (var form in MdiChildren)
            {
                form.Left = (ClientRectangle.Width - form.Width) / 2;
                form.Top = (ClientRectangle.Height - form.Height) / 2;
            }

        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            _juego.Close();
            _juego.Dispose();
            _juego = null;
            _userLogueado = null;
            btnLogin.Visible = false;
            btnCerrarSesion.Visible = false;
            btnJuego.Visible = false;

            MostrarLogin();
        }

        public void IniciarSesion(Usuario user)
        {
            _juego = null;
            _userLogueado = user;
            btnLogin.Visible = false;
            btnCerrarSesion.Visible = true;
            btnJuego.Visible = true;

            MostrarJuego();
        }

        public void MostrarRegistroUsuario()
        {
            if (_registro?.Visible == true) return;

            if (_registro == null)
            {
                _registro = new frmRegistro();
                _registro.MdiParent = this;
            }

            _login?.Hide();
            _juego?.Hide();
            _ranking?.Hide();
            _registro.Show();
        }

        private void btnRanking_Click(object sender, EventArgs e)
        {
            MostrarRanking();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            MostrarLogin();
        }

        private void btnJuego_Click(object sender, EventArgs e)
        {
            MostrarJuego();
        }
    }
}
