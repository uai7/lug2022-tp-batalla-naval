﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class ucTablero : UserControl
    {
        private Tablero _tablero;
        private List<ucCelda> _ucCeldas = new List<ucCelda>();
        private TableroType _tableroType;


        public ucTablero(Tablero tablero, TableroType type)
        {
            InitializeComponent();

            _tablero = tablero;
            _tableroType = type;



            Label lblTitulo = new Label();
            lblTitulo.AutoSize = true;
            lblTitulo.Font = new Font("Bahnschrift SemiCondensed", 14, FontStyle.Regular);
            lblTitulo.Text = type == TableroType.DEFENSA ? "Tablero Defensa" : "Tablero Ataque";

            Controls.Add(lblTitulo);

            // Tamaño de celdas
            Size size = new Size(30, 30);
            int espacioEntreCeldas = 1;
            Point startLocation = new Point(0, 30);

            for (int x = 1; x <= Partida.TableroSize; x++)
            {

                int curentX = startLocation.X + (x) * size.Height + (x) * espacioEntreCeldas;

                for (int y = 1; y <= Partida.TableroSize; y++)
                {

                    if (y == 1)
                    {
                        int lblColumnaX = startLocation.X + (x) * size.Width + (x) * espacioEntreCeldas;
                        int lblColumnaY = startLocation.Y + (y) * espacioEntreCeldas;

                        Label lblColumna = new Label();
                        lblColumna.Size = size;
                        lblColumna.AutoSize = false;
                        lblColumna.TextAlign = ContentAlignment.MiddleCenter;
                        lblColumna.Text = x.ToString();
                        lblColumna.Font = new Font("Bahnschrift SemiCondensed", 12, FontStyle.Bold);
                        lblColumna.Location = new Point(lblColumnaX, lblColumnaY);

                        Controls.Add(lblColumna);
                    }

                    if (x == 1)
                    {
                        int lblFilaX = startLocation.X + (x - 1) * size.Width + (x) * espacioEntreCeldas;
                        int lblFilaY = startLocation.Y + (y) * size.Height + (y) * espacioEntreCeldas;

                        Label lblFila = new Label();
                        lblFila.Size = size;
                        lblFila.AutoSize = false;
                        lblFila.TextAlign = ContentAlignment.MiddleCenter;
                        lblFila.Text = Convert.ToChar('A' + y - 1).ToString();
                        lblFila.Font = new Font("Bahnschrift SemiCondensed", 12, FontStyle.Bold);
                        lblFila.Location = new Point(lblFilaX, lblFilaY);

                        Controls.Add(lblFila);
                    }


                    int curentY = startLocation.Y + (y) * size.Width + (y) * espacioEntreCeldas;

                    Celda celda = _tablero.Celdas.Find(c => c.Coordenada.X == x && c.Coordenada.Y == y);

                    ucCelda _ucCelda = new ucCelda(celda, _tableroType);
                    _ucCelda.Name = "pb_" + x.ToString() + "_" + y.ToString();

                    _ucCelda.Location = new Point(curentX, curentY);
                    _ucCelda.BorderStyle = BorderStyle.None;

                    _ucCeldas.Add(_ucCelda);
                }
            }

            this.Controls.AddRange(_ucCeldas.ToArray());

        }

        public void Refrescar()
        {
            _ucCeldas.ForEach(x => x.Refrescar());
        }
    }
}
