﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsLayer
{
    public partial class ucFlota : UserControl
    {
        private List<ucBarco> _ucBarcos;
        private List<Barco> _barcos;

        public ucFlota(List<Barco> barcos)
        {
            InitializeComponent();

            _barcos = barcos;
            _ucBarcos = new List<ucBarco>();

            int currentX = 0;
            for (int i = 0; i < barcos.Count; i++)
            {
                Barco b = barcos[i];
                ucBarco uc = new ucBarco(b);

                uc.Location = new Point(currentX, 60);

                currentX += b.Largo * 30 + 20;

                _ucBarcos.Add(uc);
            }


            this.Controls.AddRange(_ucBarcos.ToArray());

        }

        public void Refrescar()
        {
            frmJuego frm = (frmJuego)this.Parent;

            foreach (ucBarco uc in _ucBarcos)
            {
                if (uc.Barco.EstaColocado && frm.BarcoSeleccionado != uc.Barco)
                {
                    uc.Visible = false;
                }
                else
                {
                    uc.Visible = true;
                }
            }

        }

        private void ucFlota_Load(object sender, EventArgs e)
        {
            Label lblTitulo = new Label();
            lblTitulo.AutoSize = true;
            lblTitulo.Font = new Font("Bahnschrift SemiCondensed", 14, FontStyle.Bold);
            lblTitulo.Text = "Flota";
            Controls.Add(lblTitulo);

            Label lblSubTitulo = new Label();
            lblSubTitulo.AutoSize = true;
            lblSubTitulo.Font = new Font("Bahnschrift SemiCondensed", 10, FontStyle.Regular);
            lblSubTitulo.Text = "Seleccione los barcos y coloquelos en la ubicación deseada utilizando Click Izquierdo.";
            lblSubTitulo.Location = new Point(0, lblTitulo.Size.Height);
            Controls.Add(lblSubTitulo);

            Label lblSubTitulo2 = new Label();
            lblSubTitulo2.AutoSize = true;
            lblSubTitulo2.Font = new Font("Bahnschrift SemiCondensed", 10, FontStyle.Regular);
            lblSubTitulo2.Text = "Alterne la orientación de los barcos utilizando Click Derecho";
            lblSubTitulo2.Location = new Point(0, lblSubTitulo.Location.Y + lblSubTitulo.Size.Height);
            Controls.Add(lblSubTitulo2);


            Button btnIniciarPartida = new Button();
            btnIniciarPartida.Size = new Size(200, 30);
            btnIniciarPartida.AutoSize = false;
            btnIniciarPartida.Text = "INICIAR PARTIDA";
            btnIniciarPartida.Font = new Font("Bahnschrift SemiCondensed", 12, FontStyle.Bold);
            btnIniciarPartida.Location = new Point(Size.Width - 200, 0);
            btnIniciarPartida.Click += btnIniciarPartida_Click;

            Controls.Add(btnIniciarPartida);
        }

        private void btnIniciarPartida_Click(object sender, System.EventArgs e)
        {
            if(!_barcos.Aggregate(true, (accum, current) => accum && current.EstaColocado))
            {
                MessageBox.Show("Coloque todos los barcos en el Tablero Defensa para poder iniciar la partida!");
                return;
            }

            frmJuego frm = (frmJuego)this.Parent;
            frm.IniciarPartida();
        }
    }
}
