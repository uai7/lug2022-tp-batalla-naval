﻿using BE;
using System;
using System.Linq;

namespace Consola
{
    internal class Program
    {
        static Random rand = new Random();

        static void Main(string[] args)
        {
            Partida p = new Partida();

            foreach (Barco b in p.Jugador1.Barcos)
            {
                bool colocado = false;

                while (!colocado)
                {
                    Coordenada c = new Coordenada(rand.Next(1, Partida.TableroSize), rand.Next(1, Partida.TableroSize));
                    Orientacion o = rand.Next(0, 2) == 0 ? Orientacion.Horizontal : Orientacion.Vertical;

                    colocado = p.Jugador1.ColocarBarco(b, c, o);

                }

            }


            foreach (Barco b in p.Jugador2.Barcos)
            {
                bool colocado = false;

                while (!colocado)
                {
                    Coordenada c = new Coordenada(rand.Next(1, Partida.TableroSize), rand.Next(1, Partida.TableroSize));
                    Orientacion o = rand.Next(0, 2) == 0 ? Orientacion.Horizontal : Orientacion.Vertical;

                    colocado = p.Jugador2.ColocarBarco(b, c, o);

                }

            }

            string msg = "";
            while (true)
            {
                OutputBoards("Jugador1", p.Jugador1);
                OutputBoards("Jugador2", p.Jugador2);



                Console.Write("Turnos: ");
                foreach (Coordenada c in p.Turnos)
                {
                    Console.Write(c + " ");
                }

                Console.WriteLine("");
                Console.WriteLine(msg);

                Console.Write("Ingrese la celda a atacar (Ej A:4) => ");
                string x = Console.ReadLine();

                try
                {
                    int row = Convert.ToInt32(x.Split(':')[0].ToCharArray().First()) - 64;
                    int col = int.Parse(x.Split(':')[1]);

                    if (row < 1 || row > Partida.TableroSize || col < 1 || col > Partida.TableroSize)
                    {
                        throw new Exception("Celda incorrecta");
                    }

                    msg = p.Atacar(new Coordenada(col, row));

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.ReadLine();
                }

                Console.Clear();
            }



            Console.ReadLine();

        }

        static void OutputBoards(string nombreJugador, Jugador j)
        {
            char[] rowLetters = Enumerable.Range(0, Partida.TableroSize).Select(i => Convert.ToChar('A' + i)).ToArray();
            string colName = Enumerable.Range(1, Partida.TableroSize).Aggregate("", (acc, curr) => acc + curr.ToString().PadRight(2));

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(nombreJugador);
            Console.WriteLine("");

            Console.Write(colName.PadLeft(colName.Length + 4, ' '));
            Console.Write("        ");
            Console.Write(colName.PadLeft(colName.Length + 4, ' '));
            Console.WriteLine("");

            for (int row = 1; row <= Partida.TableroSize; row++)
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;

                Console.Write(rowLetters[row - 1].ToString().PadLeft(3, ' ') + " ");



                for (int col = 1; col <= Partida.TableroSize; col++)
                {
                    Celda celda = j.TableroDefensa.ObtenerCelda(new Coordenada(col, row));

                    Console.BackgroundColor = ConsoleColor.Cyan;
                    Console.ForegroundColor = GetColorForLetter(celda);
                    Console.Write(celda.Letra() + " ");
                }

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("        ");

                Console.Write(rowLetters[row - 1].ToString().PadLeft(3, ' ') + " ");

                for (int col = 1; col <= Partida.TableroSize; col++)
                {
                    Celda celda = j.TableroAtaque.ObtenerCelda(new Coordenada(col, row));

                    Console.BackgroundColor = ConsoleColor.Cyan;
                    Console.ForegroundColor = GetColorForLetter(celda);
                    Console.Write(celda.Letra() + " ");
                }

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");

            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
            Console.WriteLine("");
        }

        static ConsoleColor GetColorForLetter(Celda celda)
        {
            if (celda.Atacada) return ConsoleColor.DarkRed;

            switch (celda.Letra())
            {
                case "B":
                case "F":
                case "P":
                case "S":
                case "A":
                    return ConsoleColor.Black;
                case "O":
                default:
                    return ConsoleColor.DarkGray;
            }
        }
    }



}
