﻿using System.Collections.Generic;
using System.Linq;

namespace BE
{
    public class Jugador
    {
        private Tablero _tableroDefensa;
        private Tablero _tableroAtaque;
        private List<Barco> _barcos;
        private Usuario _user;

        public Jugador(List<Barco> barcos)
        {
            _tableroDefensa = new Tablero();

            _tableroAtaque = new Tablero();

            Barcos = barcos;
        }

        public Tablero TableroDefensa { get => _tableroDefensa; }
        public Tablero TableroAtaque { get => _tableroAtaque; }
        public List<Barco> Barcos { get => _barcos; set => _barcos = value; }

        public bool EstaListo { get => Barcos.Aggregate(true, (accum, current) => accum && current.EstaColocado); }
        public Usuario User { get => _user; set => _user = value; }
        public string Nombre { get => _user == null ? "PC" : _user.Username; }

        public bool ColocarBarco(Barco barco, Coordenada coord, Orientacion orientacion)
        {
            // El barco no corresponde al de este jugador
            if (!_barcos.Contains(barco)) return false;

            // El barco ya esta colocado
            if (barco.EstaColocado) return false;

            // Checkeo que la coordenada y el largo del barco no se excedan del tamaño del tablero
            if (orientacion == Orientacion.Vertical && coord.Y + (barco.Largo - 1) > Partida.TableroSize) return false;
            if (orientacion == Orientacion.Horizontal && coord.X + (barco.Largo - 1) > Partida.TableroSize) return false;


            // Calculo las coordenadas que va a ocupar el barco en base a la
            // coordenada de inicio y la orientacion
            List<Coordenada> coordenadasAOcupar = new List<Coordenada>();

            coordenadasAOcupar.Add(coord);
            if (orientacion == Orientacion.Vertical)
            {
                for (int i = coord.Y + 1; i < coord.Y + barco.Largo; i++)
                {
                    coordenadasAOcupar.Add(new Coordenada(coord.X, i));
                }
            }
            else if (orientacion == Orientacion.Horizontal)
            {
                for (int i = coord.X + 1; i < coord.X + barco.Largo; i++)
                {
                    coordenadasAOcupar.Add(new Coordenada(i, coord.Y));
                }
            }

            // Busco las celdas que se van a ocupar
            List<Celda> celdas = coordenadasAOcupar.Select(c => TableroDefensa.ObtenerCelda(c)).ToList();

            // Checkeo que todas las celdas no esten ocupadas
            bool celdasDisponibles = celdas.Aggregate(true, (acum, x) => acum && x.Barco == null);
            if (!celdasDisponibles) return false;

            barco.Orientacion = orientacion;
            barco.CeldasOcupadas = celdas;
            celdas.ForEach(x => x.Barco = barco);

            return true;
        }

        public void RemoverBarco(Barco b)
        {
            b.CeldasOcupadas.ForEach(x => x.Barco = null);
            b.CeldasOcupadas = new List<Celda>();
        }

    }
}