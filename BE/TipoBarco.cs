﻿namespace BE
{
    public enum TipoBarco
    {
        BATTLESHIP,
        PORTAVIONES,
        DESTRUCTOR,
        SUBMARINO,
        PATRULLA,
    }
}