﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Usuario
    {
        private int _id;

        private string _username;

        private string _pass;

        private int _partidasJugadas;

        private int _partidasGanadas;

        private int _tiempoJugado;

        public int Id { get => _id; set => _id = value; }
        public string Username { get => _username; set => _username = value; }
        public string Pass { get => _pass; set => _pass = value; }
        public int PartidasJugadas { get => _partidasJugadas; set => _partidasJugadas = value; }
        public int PartidasGanadas { get => _partidasGanadas; set => _partidasGanadas = value; }
        public int TiempoJugado { get => _tiempoJugado; set => _tiempoJugado = value; }
        public string TiempoJugadoString
        {
            get
            {
                TimeSpan ts = TimeSpan.FromMilliseconds(_tiempoJugado);

                return Math.Floor(ts.TotalHours).ToString().PadLeft(2, '0') 
                    + " : " + ts.Minutes.ToString().PadLeft(2, '0') 
                    + " : " + ts.Seconds.ToString().PadLeft(2, '0');

            }
        }
    }
}
