﻿using System.Collections.Generic;

namespace BE
{
    public class Tablero
    {
        private List<Celda> _celdas;
        public List<Celda> Celdas { get => _celdas; }

        public Tablero()
        {
            _celdas = new List<Celda>();

            for (int i = 1; i <= Partida.TableroSize; i++)
            {
                for (int j = 1; j <= Partida.TableroSize; j++)
                {
                    _celdas.Add(new Celda(i, j));
                }
            }
        }

        public Celda ObtenerCelda(Coordenada c)
        {
            return _celdas.Find(e => e.Coordenada.X == c.X && e.Coordenada.Y == c.Y);
        }
    }
}