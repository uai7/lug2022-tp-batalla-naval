﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BE
{
    public class Partida
    {
        public static int TableroSize = 10;

        private EstadoPartida _estado;

        private DateTime _startTime;
        private DateTime _endTime;

        private Jugador _jugador1;

        private Jugador _jugador2;

        public EstadoPartida Estado { get => _estado; }
        public DateTime StartTime { get => _startTime; }
        public DateTime EndTime { get => _startTime; }
        public TimeSpan Duracion { get => _endTime - _startTime; }

        public Jugador Jugador1 { get => _jugador1; }
        public Jugador Jugador2 { get => _jugador2; }

        private List<Turno> _turnos = new List<Turno>();

        public List<Turno> Turnos { get => _turnos; }

        public Jugador JugadorAtacando { get => _turnos.Count % 2 == 0 ? Jugador1 : Jugador2; }

        public Jugador JugadorDefendiendo { get => _turnos.Count % 2 == 1 ? Jugador1 : Jugador2; }

        private Jugador _jugadorGanador;
        public Jugador JugadorGanador { get => _jugadorGanador; }

        public Partida()
        {
            List<Barco> barcosJ1 = CrearBarcos();
            _jugador1 = new Jugador(barcosJ1);

            List<Barco> barcosJ2 = CrearBarcos();
            _jugador2 = new Jugador(barcosJ2);

            _estado = EstadoPartida.PREPARACION;
        }

        private List<Barco> CrearBarcos()
        {
            List<Barco> barcos = new List<Barco>();

            barcos.Add(new Barco(TipoBarco.BATTLESHIP, 5));
            barcos.Add(new Barco(TipoBarco.PORTAVIONES, 4));
            barcos.Add(new Barco(TipoBarco.DESTRUCTOR, 3));
            barcos.Add(new Barco(TipoBarco.SUBMARINO, 2));
            barcos.Add(new Barco(TipoBarco.PATRULLA, 2));

            return barcos;
        }

        public bool IniciarPartida()
        {
            if (_estado == EstadoPartida.PREPARACION && Jugador1.EstaListo && Jugador2.EstaListo)
            {
                _startTime = DateTime.Now;
                _estado = EstadoPartida.EN_CURSO;
                return true;
            }

            return false;
        }

        public ResultadoTurno? Atacar(Coordenada coor)
        {
            if(_estado != EstadoPartida.EN_CURSO)
            {
                return null;
            }

            if (JugadorAtacando.TableroAtaque.ObtenerCelda(coor).Atacada)
            {
                return null;
            }

            ResultadoTurno resultado;
            Celda celdaAtaque = JugadorAtacando.TableroAtaque.ObtenerCelda(coor);
            celdaAtaque.Atacada = true;

            Celda celdaDefensa = JugadorDefendiendo.TableroDefensa.ObtenerCelda(coor);
            celdaDefensa.Atacada = true;

            if (celdaDefensa.Barco != null)
            {
                resultado = ResultadoTurno.TOCADO;

                if (celdaDefensa.Barco.CeldasOcupadas.Aggregate(true, (accum, curr) => accum && curr.Atacada))
                {
                    resultado = ResultadoTurno.HUNDIDO;
                }
            }
            else
            {
                resultado = ResultadoTurno.AGUA;
            }

            celdaAtaque.ResultadoAtaque = resultado;

            Turno turno = new Turno(JugadorAtacando, coor, resultado);
            _turnos.Add(turno);

            CheckearJugadorGanador();
            if (JugadorGanador != null)
            {
                _endTime = DateTime.Now;
                _estado = EstadoPartida.FINALIZADA;

                if(Jugador1.User != null)
                {
                    Jugador1.User.PartidasJugadas++;
                    if(Jugador1 == JugadorGanador) Jugador1.User.PartidasGanadas++;
                    Jugador1.User.TiempoJugado += (int)Duracion.TotalMilliseconds;
                }


                if (Jugador2.User != null)
                {
                    Jugador2.User.PartidasJugadas++;
                    if (Jugador2 == JugadorGanador) Jugador1.User.PartidasGanadas++;
                    Jugador2.User.TiempoJugado += (int)Duracion.TotalMilliseconds;
                }
            }

            return resultado;
        }

        private void CheckearJugadorGanador()
        {
            // J1 gana si hundio todos los barcos del J2 
            bool ganoJugador1 = _jugador2.Barcos.Aggregate(true, (accum, curr) => accum && curr.EstaHundido);

            if (ganoJugador1)
            {
                _jugadorGanador = _jugador1;
            }

            // J2 gana si hundio todos los barcos del J1 
            bool ganoJugador2 = _jugador1.Barcos.Aggregate(true, (accum, curr) => accum && curr.EstaHundido);

            if (ganoJugador2)
            {
                _jugadorGanador = _jugador2;
            }
        }
    }
}
