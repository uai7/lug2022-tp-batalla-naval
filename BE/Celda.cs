﻿namespace BE
{
    public class Celda
    {
        private Coordenada _coordenada;
        private Barco _barco;
        private bool _atacada;
        private ResultadoTurno? _resultadoAtaque;


        public Celda(int x, int y)
        {
            _coordenada = new Coordenada(x, y);
        }

        public Coordenada Coordenada { get => _coordenada; }
        public Barco Barco { get => _barco; set => _barco = value; }
        public bool Atacada { get => _atacada; set => _atacada = value; }
        public ResultadoTurno? ResultadoAtaque { get => _resultadoAtaque; set => _resultadoAtaque = value; }

        public string Letra()
        {
            return _barco != null ? _barco.Tipo.ToString()[0].ToString() : Atacada ? "X" : "O";
        }
    }
}