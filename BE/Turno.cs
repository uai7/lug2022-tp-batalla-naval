﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Turno
    {
        private Jugador _jugador;
        private Coordenada _coordenada;
        private ResultadoTurno _resultado;
        private DateTime _date;

        public Turno(Jugador jugador, Coordenada coordenada, ResultadoTurno resultado)
        {
            _jugador = jugador;
            _coordenada = coordenada;
            _resultado = resultado;

            _date = DateTime.Now;
        }

        public Jugador Jugador { get => _jugador; }
        public Coordenada Coordenada { get => _coordenada; }
        public ResultadoTurno Resultado { get => _resultado; }
        public DateTime Date { get => _date; }


    }
}
