﻿using System.Collections.Generic;
using System.Linq;

namespace BE
{
    public class Barco
    {
        private TipoBarco _tipo;
        private int _largo;
        private Orientacion _orientacion;
        private List<Celda> _celdasOcupadas;

        public Barco(TipoBarco tipo, int largo)
        {
            _tipo = tipo;
            _largo = largo;

            _celdasOcupadas = new List<Celda>();
        }

        public TipoBarco Tipo { get => _tipo; }
        public int Largo { get => _largo; }
        public Orientacion Orientacion { get => _orientacion; set => _orientacion = value; }
        public List<Celda> CeldasOcupadas { get => _celdasOcupadas; set => _celdasOcupadas = value; }
        public bool EstaColocado { get => CeldasOcupadas.Count == Largo; }
        public bool EstaHundido { get => CeldasOcupadas.Aggregate(true, (accum, curr) => accum && curr.Atacada); }

    }
}