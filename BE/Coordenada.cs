﻿using System;

namespace BE
{
    public class Coordenada
    {
        private int _x;
        private int _y;

        public Coordenada(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public int X { get => _x; }
        public int Y { get => _y; }

        public override bool Equals(object obj)
        {
            return obj != null
                && obj.GetType() == this.GetType()
                && (obj as Coordenada).X == this.X
                && (obj as Coordenada).Y == this.Y;
        }

        public override string ToString()
        {
            return Convert.ToChar('A' + Y - 1).ToString() + ":" + X.ToString();
        }
    }
}