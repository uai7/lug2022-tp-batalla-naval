﻿namespace BE
{
    public enum EstadoPartida
    {
        PREPARACION,
        EN_CURSO,
        FINALIZADA,
    }
}