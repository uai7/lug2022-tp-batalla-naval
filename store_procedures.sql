CREATE PROC usuario_insertar
@user varchar(50),
@pass varchar(50)
as
begin 
declare @id int 
set @id = (select isnull(max(id),0) + 1 from usuario)
insert into usuario (id, username, pass) values (@id, @user, @pass)
end
go


CREATE PROC usuario_buscar_por_username
@user varchar(50)
as
begin 
select id, username, pass, partidas_jugadas, partidas_ganadas, tiempo_jugado 
from usuario where username = @user 
end
go

CREATE PROC usuario_editar
@id int ,
@jugadas int,
@ganadas int,
@tiempo int
as
begin 
update usuario set
partidas_jugadas = @jugadas,
partidas_ganadas = @ganadas,
tiempo_jugado = @tiempo
where id = @id
end
go

CREATE PROC usuario_listar
as
begin 
select id, username, partidas_jugadas, partidas_ganadas, tiempo_jugado
from usuario
order by partidas_ganadas desc
end
go
