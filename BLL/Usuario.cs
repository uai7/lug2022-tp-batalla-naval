﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Usuario
    {
        DAL.Mp_Usuario mp = new DAL.Mp_Usuario();
        public BE.Usuario BuscarPorUsername(string username)
        {
            return mp.BuscarPorUsername(username);

        }

        public List<BE.Usuario> Listar()
        {
            return mp.Listar();

        }

        public int Grabar(BE.Usuario user)
        {
            if (user.Id == 0)
            {
                return mp.Insertar(user);
            } 
            else
            {
                return mp.Editar(user);
            }
        }

    }
}
